import { Component } from '@angular/core';

//import { AboutPage } from '../about/about';
//import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { FaqsPage } from '../faqs/faqs';
import { AccountsPage } from '../accounts/accounts';
import { InboxPage } from '../inbox/inbox';
import { ReportPage } from '../report/report';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FaqsPage;
  tab3Root = ReportPage;
  tab4Root = InboxPage;
  tab5Root = AccountsPage;

  constructor() {

  }
}
